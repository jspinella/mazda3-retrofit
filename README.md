# 2017 Mazda3 AA/CarPlay Retrofit
Tailored to the 2017 Mazda3 North America model with automatic transmission (slightly different procedure for manual transmission)

## CMU Update to v70
<span style="color:red">**! strongly suggested you copy the files to the flash drive from a Windows computer**  
<span style="color:red">**! strongly suggested you use a flash drive from a reliable brand (like SanDisk)**
### At a glance
- Download NA version of v70 firmware ("failsafe" and "reinstall" versions)
- Verify MD5 checksums of each
- Install v70
  
    
### Detailed guide

#### Downloading the v70 firmware


#### Loading the v70 firmware onto a flash drive
Simply copy the two v70 firmware files onto the root of the flash drive. Yeah, the root. The flash drive should be FAT32 formatted.

 We'll check the MD5 SHA1 hash once the v70 files are on the flash drive since those are the exact copies of the files the CMU will be flashing.

#### Checking the MD5 SHA1 Hashes (Checksums)
I use Microsoft's official tool, [FCIV](https://support.microsoft.com/en-us/help/889768/how-to-compute-the-md5-or-sha-1-cryptographic-hash-values-for-a-file), available [here](https://support.microsoft.com/en-us/help/841290). Chrome for example is great about downloading files, and should tell you if the download was corrupted. I would guess > 99% of the time, if your file downloads correctly, you are fine to flash it, however since the CMU is expensive and difficult to replace if bricked, and checking the checksum only takes a minute, I *strongly suggest* you do it.

cmu150_NA_70.00.100A_failsafe  
**9324D1907C2C722738B0A389DE73E91D DB04E842A797AC20449853ED63CBE38821A76162**  
cmu150_NA_70.00.100A_reinstall  
**DA7667967E62E324C4EF457DE4A262BF		8C0C6608B229A1079D8F467816687B900D996EA7**  
  
If you find there is a mismatch, honestly odds are much more likely that you have a slightly different version of the v70 firmware, maybe a newer version, than an actual botched download. Double check that you have downloaded v70...100A of the North America (NA) firmware. If you have downloaded a newer version, well hello from the past! Do we have hover Mazdas yet? Anyway, just try to find the MD5 checksum for *your* version of the firmware.

#### Installing the firmware
Please use the Official Mazda PDF ("Worldwide Update Procedure"), it's clearer/easier to read than markdown could ever be. At least as of this writing, it is available [here](https://my.hidrive.com/share/hsodpqja.l#$/Mazda%20Firmware/NA%20N). It is also in the Docs folder where this README is located, as I find that link is often dead.

Notes on the firmware install (at least in my case):
- Upon starting the reinstall installation process, I briefly got that "update failed" message, certainly freaked me out, but it went away after a couple of seconds and the update screen with the progress bar came up.  
- The v70 reinstall may get "stuck" on 2%... just be patient, the bar moves at different rates throughout the process, and it does in fact take about 35 minutes... just spends 5-10 minutes at 2%.
- Using a flash drive with an activity light is just another way you can see that the update is in fact progressing. It's nice to have.
- After the install, I noticed some of my settings were reset. I don't recall all of the changes, but one of them was the Smart City Brake Support warning distance setting, and another was the height of the ADD (active driving display). 
- I ALWAYS take a picture of the firmware about screen before starting the aux module replacement. This way, there is no doubt down the line about whether the firmware versions were v70+ should something go awry after the aux module replacement.

<img src="./Pictures/1.jpeg" alt="Firmware info screen" title="OS Version and Fail-Safe Version are both v70" style="margin: 0 auto; display: block; width: 90%; max-width: 400px;" />

## Replacing the Aux module
<span style="color:red">**! must be on v70 firmware**

### At a glance
- Remove glove box
- Remove front trim where center vents are
- Remove CMU bolt and pull that guy out
- Remove center console components (front, middle and rear sections)
- Remove trim near driver's and passenger's feet behind center console
- Pop out the old Aux Module from behind
- Run the new cables from the CMU to the Aux Module area
- Put the new Aux Module in
- Put everything back together again!

This guide is curtailed for the 2017 Mazda3, and starting in 2017, Mazda put the Aux module under the climate control area, rather than in that compartment in the center console. You would think that would make it easier, but actually it is more difficult... instead of "loosening" the center console, we actually have to detach it to the point where we can pull it away from the climate control area enough to reach behind the Aux Module so we can remove the Aux Module. This may not be totally clear, but you will see what I mean soon enough....

### Glove Box
There are many YouTube videos that cover this. It doesn't matter which year your car is, 2014-2017, it's the same. Disconnect the hydraulic piece, then push into either side towards the back to pop out the two pieces holding the glove box on the track.   
    
      
Reassembly is a little bit trickier. You must insert the glove box into the track near your feet, where the "bottom" of the glove box is. To do this, there can't really be anything on the floor, you have to get it pretty low, basically in a position where it would be like 1.5x as open as it normally would open to. Then, work it into the tracks on either side. Once it is in place, add the hydraulic piece back, using the hole closest to you to place said hydraulic piece. There are two, the further away one and the slightly closer one. You want to use the slightly closer one. 

### Front Trim (silver piece)
Guitar picks work for this, but I would have probably had an easier time with larger plastic tools. I wrapped all prying tools/guitar picks with painter's tape to prevent damage. Worked like a charm. I used two prying tools, a guitar pick and an iTool thing from an iPhone display replacement kit, to pry on *either side* of each tab. This seemed to be the most effective way of prying the tabs loose. 

<img src="./Pictures/4.jpeg" alt="The hard to get to tab" title="The hard to get to tab" style="margin: 0 auto; display: block; width: 90%; max-width: 400px;" />
  
Start with the tab next to the far right end of the silver/chrome trim piece. Do not start prying elsewhere until this tab/clip is pried up. Then continue to the next clip, and then the next one. For getting the vent part out, I pry a bit under the non-vent part, next to where the vents are, while also pulling a bit on the vent part with my fingers. It takes some working, but I eventually work this guy free. Be careful as the clips are metal and will scratch things if you let them. Be sure to detach the Emergency Lights button cable from behind the vent area before you move the piece away from the dash.  

I've found it's not worth covering the dash around this trim piece with protective painter's tape, as it is rather difficult to damage the dash with these tabs. They are metal, but knowing about them and being somewhat aware of what you're doing is sufficient to not scratch the dash with them.

<img src="./Pictures/5.jpeg" alt="The hard to get to tab" title="I go at this last tab at an angle" style="margin: 0 auto; display: block; width: 90%; max-width: 400px;" />

I would not recommend trying to pry the last tab right above it... The clearance is tight, and you will likely damage the plastic trim. Instead, go at it from an angle (pictured).

<img src="./Pictures/3.jpeg" alt="The hard to get to tab" title="The hard to get to tab" style="margin: 0 auto; display: block; width: 90%; max-width: 400px;" />

I like to use a microfiber cloth as a buffer between the metal piece where the bolt holds the CMU in, and the plastic of the dash. It's kind of sharp and sticks out below the CMU so it is difficult to see.

<img src="./Pictures/2.jpeg" alt="CMU revealed" title="Lay down a microfiber cloth to prevent damaging the plastic" style="margin: 0 auto; display: block; width: 90%; max-width: 400px;" />
  
### CMU
Remove the single bolt holding the CMU in place. Once it is out, you can simply pull on the dash part of the CMU assembly until about 2 inches of the back is exposed. From there, you must disconnect the 5 or so (sic) cables connected to the CMU. They won't go anywhere, so no need to worry about them falling once you disconnect them.   

 
### Center Console
The center console has three sections - the gear shifter, the cupholder/e brake area, and the rear compartment. The rear compartment actually includes the sides of the center console (with the textured seat-matching vinyl pieces) extending to the front part of the center console. 

#### Middle Section (cupholders)
Open the accordion-style cupholder door.

Remove the bottom liners in each cupholder, and remove the single screw under one of them.

Open the compartment in the back section of the center console area.

Pry the middle section off per the *2017 Mazda3 Trim Removal* document.

<img src="./Pictures/7.jpeg" alt="Inside the middle part of the center console" title="Unravel the electrical tape to free the wires from the plastic pieces" style="margin: 0 auto; display: block; width: 90%; max-width: 400px;" />

Unwrap the electrical tape holding the DC power outlet cable to the plastic trim of the center console. You can reuse this tape upon reassembly if you are careful about unwrapping it, but fresh electrical tape will do as well. 

The same will need to be done to the identical plastic piece and wire for the command knob (very top of picture).

#### Back Section (includes the sides of middle section)


#### Front Section (gear shifter/ glossy black trim)
Start by removing the button on the front of the gear shifter lock knob (the thing you push to move the shifter). I do this with a guitar pick wrapped with painter's tape. Stick it in the top as much as you easily can, and push up in the motion indicated by the *2017 Mazda3 Trim Removal* document. In my experience, it varies by car how easily this piece comes out. 

Once it is out, find the hook that holds the shifter knob to the shifter. Without a special tool, say a plastic tool with a small hook, it is very difficult to remove this piece. I use a small zip tie, and spend about 5 minutes trying to loop it through the hook piece. Once it is through, I complete the zip tie loop and use it to pull the hook out. It doesn't take much force at all.

Pull the knob up and away from the shifter. 

Now is time to remove the piano/glossy black trim piece on top of the gear shifter. Refer to the *2017 Mazda3 Trim Removal* document. **Note the order in which you should pry up on the corners**. **I suggest starting from the Driver's side** so that you can pull up on the bottom left corner (near the Sport mode switch) more easily than if you were in the passenger seat. This may have been my achilles heel for cracking the glossy black outer trim piece. I have also found it difficult in general to pull up these pieces one tab/notch at a time, they tend to just come up all at once, which makes it difficult to pry the gear shifter trim piece one corner at a time in the order the document specifies. 

<img src="./Pictures/13.jpeg" alt="Weak spot of the gear shifter trim" title="Don't get got boys, pull from this corner first!" style="margin: 0 auto; display: block; width: 90%; max-width: 400px;" />

I have not been able to successfully pull the gear shifter trim piece off without cracking the outer part of it. As of now, a replacement piece is only available as part of a set that includes way more pieces than you actually need, and runs about $200. If you need this piece, or want to be able to purchase this piece standalone, Mazda needs to offer it standalone to the many resellers out there. I strongly encourage you to reach out to Mazda (North America: 800-222-5500 Monday-Saturday) to try to make this happen. This piece costs Mazda maybe $1 to make, I'm all for paying a 1000% premium, but for just this piece, not the entire assembly..  

##Front/Side Scowling

I'm tired... soon...

## Good Tips
- Park on as flat a surface as you can, and put a cinderblock behind one or both of your rear tires. If you think you're on a flat surface, you're probably not and you risk having your car roll forward or backward into something. You can engage the parking brake too. You'll be moving the gear shifter knob a good bit and will spend a lot of time in Neutral.
- You want to test CarPlay/AA out as soon as you can, and I respect that because I am the same way. Besides, it would blow if you put everything back together to find it doesn't work. So when you do this, make sure the car is in PARK, and I would recommend having at least the gear shifter area back together, including the parking brake cable. I am not 100% sure why the parking brake automatically engaged when I went to test out CarPlay, it was either because I was not in park (probably the reason) or the parking brake cable was not connected (probably not the reason). If you do turn on ACC mode or start the car and find the parking brake is engaged/the car is beeping at you, don't panic! Simply put her in park (if she isn't already) and disengage the parking brake. The car will NOT turn off, ACC or full start, it will NOT turn off if you aren't in Park. Be in Park BEFORE you turn it on.
- I was not able to get that rubberized "phone" area between the Aux module and the gear shifter area, off. You don't have to get it off, but it will make things easier if you can.
- When removing the gear shifter piece, you are prying off at the *matte gray* plastic tabs that are actually under the piano black/glossy black tabs if you look at the thing from the back where you should start the prying. As other guides mention, just pull on it until it comes off, ease it off bit by bit, one tab at a time, but ultimately you're pulling on the thing with some force. I didn't use any prying tools or anything like that. 
- In fact the only time I used prying tools was for the dash trim part right at the beginning.
- The offical Mazda documentation is *obsessive* about saying things like "be careful here or you might break the tabs!". For what it's worth, I didn't brake a single tab. The only damage came from backing into the garage door ;)
- I had a hard time figuring out where all the screws went when it was time for reassembly. Seriously. Harder to keep track of that shit than it sounds. So here's the breakdown:
- 2 screws on either side of the middle console area by the driver's and passenger's feet (total of FOUR, they are CHROME).
- 2 screws in the bottom of the rear third of the center console (they are GRAY/BLACK)
- 1 screw in the middle third of the center console bottom/cup-holder (it is CHROME with a washer on it)
- 2 plastic tab screw things that go on either side of the middle third of the center console. They are a bit tough to get back in, I recommend a small, soft mallet padded with painter's tape
- 2 screws between the rear 3rd and center 3rd center console sections (these are GRAY/BLACK)
- 2 screws between the middle 3rd and front 3rd center console secionts (these are CHROME)
- 1x 10mm socket screw (octogon shape) for the CMU


### Reassembly
So I don't think, at least for now, I'm going to spell out every step here. Generally you're going in the opposite direction of the disassembly. My pain points were keeping track of which screws go where and getting the hook piece back into the gear shifter knob.


- Getting the hook piece out: Get a zip tie around it, feed the zip tie into its first knotch or so, so you have a secure loop, and use that loop to pull the hook piece out. It doesn't take much force, but it's tough to actually get a hold on it, hence the zip tie. 


- Getting the hook piece back in: For this one I, with the knob by itself, picked it up, had it face me, and put the hook piece back in. Remember, it goes just below that gray plastic piece towards the very bottom of that opening for the knob button. Put it in one side at a time, just kind of shove it in.. completely. Make sure it is completely in. Then, you can just put the knob back on the shifter stick. Push down on the top of the knob with some of your weight until you hear it snap into place (I tried putting the knob on the stick and then putting the hook in the knob, it did not work well at all).




### TOOLS
#### I'm using guitar picks instead of formal pry tools
- A small, unused zip tie (for removing the hook piece from the gear shifter in auto transmission models). When you reassemble this, you can put the hook back in (easier to do it when you can see it...) and then push down with some force until the knob locks onto the gear shifter lever. Reattach shifter button.
- Several guitar picks (prying tools). I believe these worked as well or better than those larger, prying tools made specificly for trim removal. The guitar picks are able to go right in between the tabs and the dash and I would think they work at least as well as those "official" pry tools.
- One of those flat-on-either-side device repair tools you get from iFixit (TODO:PICTURE)
- Painter's tape
- That 10mm socket wrench setup the other guides tell you to get, BUT you don't need 2x 8" extensions, a single 10" extension will suffice. I wouldn't go with 1x 8" as you are likely to hit things like the CMU display.
- Patience. This took 10 hours or so (a couple of short breaks in between), in part because I didn't have a single, accurate, informative guide to use. Here's hoping you can beat my record.

### After the Install
- I noticed the leather around the gear shifter was a little tight in Park, so I went for a stroll (reverse, drive, park) and it feels normal again. Just gotta work it a little ;)